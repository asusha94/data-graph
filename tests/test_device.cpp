#include <cxxtest/TestSuite.h>

#include <data-graph/engine.hpp>

// engine

using scalar_type = dg::decl_data_type<int, float, double>;

using add_op = dg::decl_operation<scalar_type(scalar_type, scalar_type)>;

using engine = dg::engine<dg::use_data_types<scalar_type>,   //
                          dg::use_operations<add_op>>;

static_assert(engine::n_data_types == 2, "Unexpected value for engine::n_types");
static_assert(engine::n_operations == 3, "Unexpected value for engine::n_operations");
static_assert(engine::max_n_subtypes == 3, "Unexpected value for engine::max_n_subtypes");
static_assert(engine::max_n_arguments == 3, "Unexpected value for engine::max_n_arguments");

// device

struct serial_device
{
    struct add_op
    {
        std::error_code
            compute(engine::value<scalar_type>& out, engine::value<scalar_type> a1, engine::value<scalar_type> a2)
        {
            // switch (a1.index())
            // {
            // case /* constant-expression */:
            //     /* code */
            //     break;

            // default:
            //     break;
            // }
            return {};
        }

    private:
        // template <typename Arg1>
        // dg::value<scalar_type> _compute(const Arg1& a1, dg::value_handle<scalar_type> a2)
        // {
        //     switch (a2.index())
        //     {
        //     case /* constant-expression */:
        //         /* code */
        //         break;

        //     default:
        //         break;
        //     }
        // }

        // template <typename Arg1, typename Arg2>
        // std::common_type_t<Arg1, Arg2> _compute(const Arg1& a1, const Arg2& a2)
        // {
        //     return a1 + a2;
        // }
    };

    template <class Operation>
    struct generator;
};

// table

template <>
struct serial_device::generator<add_op>
{
    using type = typename serial_device::add_op;
};

//

class test_device : public CxxTest::TestSuite
{
public:
    void test_graph_compilation()
    {
        /*
           [inp1]     [inp2]
               \_     _/ |
                 \   /   |
                  (+)    |
                   |     |
                   V     |
               [add_out] |
                   |     |
                  (+)<---'
                   |
                   V
                 [out]
         */
        engine::graph g;

        engine::node<scalar_type> inp1, inp2, out;
        {
            inp1 = g.identity<scalar_type>();

            inp2 = g.identity<scalar_type>();

            auto add_out = g.operation<add_op>(inp1, inp2);

            out = g.operation<add_op>(add_out, inp2);
        }

        auto d = engine::device::create<serial_device>(g);

        auto out_ready = d.compile(out, inp1, inp2);

        int v = out_ready(1, 2);

        TS_ASSERT_EQUALS(v, 0)
    }
};


#ifndef __DATA_GRAPH__INTERFACE__INLINE_HPP__
#define __DATA_GRAPH__INTERFACE__INLINE_HPP__

#ifndef __DATA_GRAPH__INTERFACE_HPP__
#    error "Please use `interface.hpp` instead. This header is not supposed to be used directly."
#endif

#include <optional>

namespace dg
{
    //
    // interface_type::INLINE
    //

    template <>
    struct interface<interface_type::INLINE>
    {
        template <class Interface>
        struct internal;

        /**
         * @class base
         */

        template <class Internal>
        struct base
        {
            using internal_type = Internal;

            inline operator bool() const noexcept;

            inline std::size_t hash() const noexcept;

        protected:
            base() = default;

            base(const base& o)     = default;
            base(base&& o) noexcept = default;

            explicit base(const internal_type& s);
            explicit base(internal_type&& s) noexcept;

            template <class... Args>
            explicit base(std::in_place_t, Args&&... args);

            ~base() = default;

            base& operator=(const base& o) = default;
            base& operator=(base&& o) noexcept = default;

            internal_type* self() noexcept;
            const internal_type* self() const noexcept;

        private:
            std::optional<internal_type> _self;

            template <class>
            friend struct internal;

            friend struct ::std::hash<base>;
        };

        /**
         * @class internal
         * @tparam Interface
         */

        template <class Interface>
        struct internal
        {
            using interface_type = Interface;
            using internal_type  = typename interface_type::internal_type;

            static internal_type* cast(interface_type& o);
            static const internal_type* cast(const interface_type& o);

        protected:
            internal()  = default;
            ~internal() = default;
        };
    };

    using interface_inline = interface<interface_type::INLINE>;
}   // namespace dg

#include "./inline/base.ipp"
#include "./inline/internal.ipp"

#endif


#ifndef __DATA_GRAPH__ENGINE__MAIN__HELPERS_IPP__
#define __DATA_GRAPH__ENGINE__MAIN__HELPERS_IPP__

#include "./helpers.hpp"

namespace dg::detail ::engine_helpers
{
    // struct is_engine

    template <class Engine>
    struct is_engine : public std::false_type
    {};

    template <class DataTypesList, class OperationsList, class Allocator>
    struct is_engine<dg::engine<DataTypesList, OperationsList, Allocator>> : public std::true_type
    {};

    //
    // common
    //

    namespace detail
    {
        template <class Engine>
        struct value_handle final
        {
            using subtype_idx_t               = typename Engine::subtype_idx_type;
            static inline constexpr auto None = subtype_idx_t(-1);

            subtype_idx_t subtype_idx = None;
            void* data_ptr            = nullptr;
        };
    }   // namespace detail
}   // namespace dg::detail::engine_helpers

#endif
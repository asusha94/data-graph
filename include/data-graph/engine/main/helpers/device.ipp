
#ifndef __DATA_GRAPH__ENGINE__MAIN__HELPERS__DEVICE_IPP__
#define __DATA_GRAPH__ENGINE__MAIN__HELPERS__DEVICE_IPP__

#include "../helpers.hpp"

namespace dg::detail::engine_helpers::device
{
    // struct has_generator

    template <class Device>
    struct has_generator : std::bool_constant<sfinae::has_nested__generator_v<Device>>
    {};

    // struct is_kernels_provider

    template <class Device>
    struct is_kernels_provider : has_generator<Device>::type
    {};
}   // namespace dg::detail::engine_helpers::device

#endif
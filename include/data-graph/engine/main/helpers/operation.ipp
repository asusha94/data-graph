
#ifndef __DATA_GRAPH__ENGINE__MAIN__HELPERS__OPERATION_IPP__
#define __DATA_GRAPH__ENGINE__MAIN__HELPERS__OPERATION_IPP__

#include "../helpers.hpp"

namespace dg::detail::engine_helpers::operation
{
    // get_predefined_types_list

    namespace _impl
    {
        using _Predefined_List_ = types_list<operation::identity, operation::condition>;
    }

    struct get_predefined_types_list
    {
        using type = _impl::_Predefined_List_;
    };

    using get_predefined_types_list_t = typename get_predefined_types_list::type;

    // get_types_list

    namespace _impl
    {
        template <class OperationsList,
                  typename = typename OperationsList::template starts_with_t<get_predefined_types_list_t>>
        struct get_types_list;

        template <class... Operations>
        struct get_types_list<types_list<Operations...>, std::false_type>
        {
            using type =
                typename get_predefined_types_list_t::template concat_t<typename types_list<Operations...>::unique_t>;
        };

        template <class... Operations>
        struct get_types_list<types_list<Operations...>, std::true_type>
        {
            using type = typename types_list<Operations...>::unique_t;
        };
    }   // namespace _impl

    template <class OperationsList>
    struct get_types_list : public _impl::get_types_list<OperationsList>
    {};

    template <class... Operations>
    struct get_types_list<use_operations<Operations...>>
        : public get_types_list<typename use_operations<Operations...>::operations>
    {};

    // get_names_table

    namespace _impl
    {
        template <class Operation, std::size_t I, typename>
        struct name_getter;

        template <class Operation, std::size_t I>
        struct name_getter<Operation, I, std::true_type>
        {
            static constexpr auto value = std::string_view(Operation::name);
        };

        template <class Operation, std::size_t I>
        struct name_getter<Operation, I, std::false_type>
        {
            using type = typename to_chars<
                base10::to_digits_t<I>>::template with_prefix<'o', 'p', 'e', 'r', 'a', 't', 'i', 'o', 'n', '_'>;

            static constexpr auto value = std::string_view(type::value, type::size());
        };

        template <class OperationsList>
        struct get_names_table;

        template <class... Operations>
        struct get_names_table<types_list<Operations...>>
        {
            template <class Indices>
            struct enumerate;

            template <std::size_t... Is>
            struct enumerate<std::index_sequence<Is...>>
            {
                // Predefined and it already has a name

                static constexpr std::array<std::string_view, sizeof...(Operations)> value = {
                    name_getter<Operations,
                                Is - get_predefined_types_list_t::size(),
                                sfinae::has_member__name_t<Operations>>::value...
                };
            };

            static constexpr auto value = enumerate<std::make_index_sequence<sizeof...(Operations)>>::value;
        };
    }   // namespace _impl

    template <class OperationsList>
    struct get_names_table
    {
        static constexpr auto value = _impl::get_names_table<get_types_list_t<OperationsList>>::value;
    };

    // get_input_types

    namespace _impl
    {
        template <class Operation, typename = sfinae::has_nested__input_types_t<Operation>>
        struct get_input_types;

        template <class Operation>
        struct get_input_types<Operation, std::false_type>
        {
            using type = types_list<>;
        };

        template <class Operation>
        struct get_input_types<Operation, std::true_type>
        {
            using type = typename Operation::input_types;
        };
    }   // namespace _impl

    template <class Operation>
    struct get_input_types
    {
        using type = typename _impl::get_input_types<Operation>::type;
    };

    template <class F>
    struct get_input_types<declarations::operation<F>>
    {
        using type = typename declarations::operation<F>::input_types;
    };

    // get_output_type

    namespace _impl
    {
        template <class Operation, typename = sfinae::has_nested__output_type_t<Operation>>
        struct get_output_type;

        template <class Operation>
        struct get_output_type<Operation, std::false_type>
        {
            using type = void;
        };

        template <class Operation>
        struct get_output_type<Operation, std::true_type>
        {
            using type = typename Operation::output_type;
        };
    }   // namespace _impl

    template <class Operation>
    struct get_output_type
    {
        using type = typename _impl::get_output_type<Operation>::type;
    };

    template <class F>
    struct get_output_type<declarations::operation<F>>
    {
        using type = typename declarations::operation<F>::output_type;
    };

    // get_max_n_arguments

    namespace _impl
    {
        template <class Operation>
        struct n_arguments_getter;

        // predifined

        template <>
        struct n_arguments_getter<identity>
        {
            static constexpr std::size_t value = 1;
        };

        template <>
        struct n_arguments_getter<condition>
        {
            static constexpr std::size_t value = 3;
        };

        // user defined

        template <class Operation>
        struct n_arguments_getter
        {
            static constexpr std::size_t value = get_input_types_t<Operation>::size();
        };

        template <class OperationsList>
        struct get_max_n_arguments;

        template <class... Operations>
        struct get_max_n_arguments<types_list<Operations...>>
        {
            static constexpr auto value = reduce_v<reduce_ops::max, 0, (n_arguments_getter<Operations>::value)...>;
        };
    }   // namespace _impl

    template <class OperationsList>
    struct get_max_n_arguments
        : std::integral_constant<std::size_t, _impl::get_max_n_arguments<get_types_list_t<OperationsList>>::value>
    {};

    // get_id

    template <class OperationsList, class Operation>
    struct get_id : std::integral_constant<
                        std::size_t,
                        engine_helpers::operation::get_types_list_t<OperationsList>::template index<Operation>()>
    {};
}   // namespace dg::detail::engine_helpers::operation

#endif

namespace dg::detail
{
    namespace _impl
    {
        template <class Engine, class DataType>
        using value_interface_base = interface_inline::base<value_base_internal<Engine, DataType>>;
    }

    template <class Engine, class DataType>
    value_base<Engine, DataType>::value_base(device_base<Engine>& dev)
    {}

    template <class Engine, class DataType>
    value_base<Engine, DataType>::value_base(const value_base& o)
    {}

    template <class Engine, class DataType>
    value_base<Engine, DataType>& value_base<Engine, DataType>::operator=(const value_base& o)
    {
        return *(this);
    }

    template <class Engine, class DataType>
    value_base<Engine, DataType>::value_base(value_base&& o) noexcept
    {}

    template <class Engine, class DataType>
    value_base<Engine, DataType>& value_base<Engine, DataType>::operator=(value_base&& o) noexcept
    {
        return *(this);
    }

    template <class Engine, class DataType>
    template <typename T, typename>
    T value_base<Engine, DataType>::_impl_get() const
    {
        return T();
    }

    template <class Engine, class DataType>
    template <typename T, typename>
    void value_base<Engine, DataType>::_impl_set(T&& t)
    {}

    template <class Engine, class DataType>
    typename Engine::subtype_idx_type value_base<Engine, DataType>::_impl_subtype_index() const
    {}
}   // namespace dg::detail

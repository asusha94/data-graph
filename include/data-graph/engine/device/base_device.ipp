
namespace dg::detail
{
    namespace _impl
    {
        template <class Engine>
        using device_interface_base = interface_unique::base<device_base_internal<Engine>>;
    }

    // device_base

    template <class Engine>
    template <class DeviceImpl>
    device_base<Engine>::device_base(graph_base& graph, DeviceImpl&& impl)
        : _impl::device_interface_base<Engine>(std::in_place, graph, std::forward<DeviceImpl>(impl))
    {}

    template <class Engine>
    device_base<Engine>::~device_base()
    {}

    template <class Engine>
    device_base<Engine>::device_base(device_base&& o) noexcept : _impl::device_interface_base<Engine>(std::move(o))
    {}

    template <class Engine>
    device_base<Engine>& device_base<Engine>::operator=(device_base&& o) noexcept
    {
        _impl::device_interface_base<Engine>::operator=(std::move(o));
        return *this;
    }

    // _impl_compile

    template <class Engine>
    template <class DataType, class... InputDataTypes>
    inline void device_base<Engine>::_impl_compile(pipeline_base<DataType(InputDataTypes...)>& out,
                                                   const node_base<DataType>& node,
                                                   node_base<InputDataTypes>&... input_nodes)
    {
        // TODO: assert (out)

        auto graph_ptr = this->self()->graph();

        auto node_ptr = node_base_internal<Engine, DataType>::cast(node);

        if (graph_ptr != node_ptr->graph()) throw std::invalid_argument("node");

        using nodes_list_iterator_t = typename graph_base_internal<Engine>::nodes_list_iterator_t;

        std::list<std::pair<std::size_t, nodes_list_iterator_t>> input_args = { std::make_pair(
            0ul, node_base_internal<Engine, InputDataTypes>::cast(input_nodes)->node_iter())... };

        static_for<sizeof...(InputDataTypes)>([&](auto i) {
            auto it = std::begin(input_args);
            std::advance(it, i);
            it->first = i;
        });

        struct operation_data
        {
            nodes_list_iterator_t node_it;
            std::vector<std::size_t> inputs;
            std::size_t arg_i = std::size_t(-1);

            operation_data() = default;
            explicit operation_data(const nodes_list_iterator_t& it) : node_it(it), inputs(0) {}
        };

        // 1. fill a list of nodes

        std::list<operation_data> pipeline_nodes_list;
        std::list<nodes_list_iterator_t> default_inputs;
        {
            using pipeline_iter_t = typename decltype(pipeline_nodes_list)::iterator;

            std::map<std::size_t, pipeline_iter_t> visited_table;

            pipeline_iter_t parent_node_pos_it = pipeline_nodes_list.end();

            std::list<nodes_list_iterator_t> stack = { node_ptr->node_iter() };
            while (!stack.empty())
            {
                auto node_it = stack.back();
                stack.pop_back();

                auto idx = graph_ptr->index_of(node_it);

                auto vis_it = visited_table.find(idx);

                if (vis_it != visited_table.end())
                {
                    if (parent_node_pos_it == pipeline_nodes_list.end())
                    {
                        // TODO : do something with this unlikely case
                        throw std::runtime_error("A node without a child looked by the child");
                    }
                    else
                    {
                        auto start = std::next(parent_node_pos_it);
                        auto end   = std::next(vis_it->second);
                        // this moves a sublist to another place inside the list with preserving of iterators.
                        pipeline_nodes_list.splice(std::begin(pipeline_nodes_list), pipeline_nodes_list, start, end);
                    }
                }
                else
                {
                    pipeline_nodes_list.emplace_front(node_it);

                    auto pipe_it = std::begin(pipeline_nodes_list);

                    visited_table.insert(std::make_pair(idx, pipe_it));

                    auto pos_it = std::find_if(std::begin(input_args), std::end(input_args), [&node_it](auto& item) {
                        return item.second == node_it;
                    });

                    if (pos_it != std::end(input_args))
                    {
                        input_args.erase(pos_it);
                        pipe_it->arg_i = pos_it->first;
                    }
                    else
                    {
                        if (node_it->in_node_idxs.empty())
                        {
                            if (!node_it->node.default_value)
                                throw std::runtime_error(
                                    "One of nodes, which is not listed, doesn't have a default value.");
                            default_inputs.push_back(node_it);
                        }
                        else
                        {
                            parent_node_pos_it = pipe_it;
                            pipe_it->inputs.reserve(node_it->in_node_idxs.size());
                            for (auto in_node_it : node_it->in_node_idxs)
                            {
                                stack.push_back(in_node_it);
                                pipe_it->inputs.push_back(graph_ptr->index_of(in_node_it));
                            }
                        }
                    }
                }
            }

            {
                auto table_end_it = std::end(visited_table);
                for (auto& item : pipeline_nodes_list)
                {
                    for (auto& idx : item.inputs)
                    {
                        auto it = visited_table.find(idx);
                        if (it == table_end_it)
                        {
                            // TODO : unexpected
                            throw std::runtime_error("A node hasn't found in the table");
                        }
                        else
                        {
                            idx = static_cast<std::size_t>(std::distance(pipeline_nodes_list.begin(), it->second));
                        }
                    }
                }
            }
        }

        // pipeline optimization
        {
            // TODO : removing identity duplicates and etc.
        }

        // fill the pipeline instance
        {
            auto out_i = pipeline_base_internal<Engine, DataType(InputDataTypes...)>::cast(out);
            using item_t = typename std::remove_pointer_t<decltype(out_i)>::operation_data;
            std::vector<item_t> ops;
            {
                ops.reserve(pipeline_nodes_list.size());
                for (auto& item : pipeline_nodes_list)
                {
                    ops.emplace_back(item.node_it->node.operation_id, std::move(item.inputs), item.arg_i);
                }
            }
            out_i->operations(std::move(ops));
        }
    }
}   // namespace dg::detail

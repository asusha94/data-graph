
#ifndef __DATA_GRAPH__DETAIL__DEVICE__HELPERS_HPP__
#define __DATA_GRAPH__DETAIL__DEVICE__HELPERS_HPP__

#include <data-graph/type_traits.hpp>

#include <data-graph/detail/engine/helpers.hpp>

namespace dg::detail
{
    template <class Engine>
    struct device_base;

    struct run_context;

    template <class Engine>
    struct device_helpers final
    {
        static_assert(engine_helpers::is_engine_v<Engine>, "Provided `Engine` is not a valid engine.");

        using engine_type = Engine;

        using device_type = typename engine_type::device;

        using operations_list = typename engine_type::operations_list;

        static constexpr auto n_operations = operations_list::size();
    };
}   // namespace dg::detail

#include "./_impl/helpers.ipp"

#endif

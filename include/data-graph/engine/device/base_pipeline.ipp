
namespace dg::detail
{
    namespace _impl
    {
        template <class Engine, class RetType, class... ArgsTypes>
        using pipeline_interface_base = interface_unique::base<pipeline_base_internal<Engine, RetType(ArgsTypes...)>>;
    }

    // pipeline_base

    template <class Engine, class RetType, class... ArgsTypes>
    pipeline_base<Engine, RetType(ArgsTypes...)>::pipeline_base(device_base<Engine>& dev)
        : _impl::pipeline_interface_base<Engine, RetType, ArgsTypes...>(std::in_place, dev)
    {}

    template <class Engine, class RetType, class... ArgsTypes>
    pipeline_base<Engine, RetType(ArgsTypes...)>::pipeline_base(pipeline_base&& o) noexcept
        : _impl::pipeline_interface_base<Engine, RetType, ArgsTypes...>(std::move(o))
    {}

    template <class Engine, class RetType, class... ArgsTypes>
    pipeline_base<Engine, RetType(ArgsTypes...)>& pipeline_base<Engine, RetType(ArgsTypes...)>::
        operator=(pipeline_base&& o) noexcept
    {
        _impl::pipeline_interface_base<Engine, RetType, ArgsTypes...>::operator=(std::move(o));
        return *this;
    }

    template <class Engine, class RetType, class... ArgsTypes>
    template <class... Args>
    inline void pipeline_base<Engine, RetType(ArgsTypes...)>::_impl_call(value_base<Engine, output_type>& out,
                                                                         Args&&... args)
    {
        if (!(*this)) throw std::runtime_error("empty object");

        using initialize_context = _impl::kernel::initialize_context<Engine>;
        using compute_context    = _impl::kernel::compute_context<Engine>;
        using finalize_context   = _impl::kernel::finalize_context<Engine>;

        auto args_tuple = std::make_tuple(std::forward<Args>(args)...);

        auto device_ptr = this->self()->device();

        std::vector<value_handle<Engine>> data_table;
        data_table.reserve(this->self()->operations().size());

        for (auto& item : this->self()->operations())
        {
            if (item.input_nodes.empty())
            {
                auto v = tuple_visit_idx<decltype(args_tuple)>(
                    [device_ptr, &args_tuple](auto idx) {
                        using I = decltype(idx);

                        auto& arg = std::get<I::value>(args_tuple);

                        using arg_type      = std::decay_t<decltype(arg)>;
                        using data_type     = typename pipeline_base::inputs_types_list::template at_t<I::value>;
                        using subtypes_list = detail::engine_helpers::data_type::get_subtypes_t<data_type>;

                        if constexpr (std::is_same_v<arg_type, value_base<Engine, data_type>>)
                        {
                            auto arg_i = value_base_internal<Engine, data_type>::cast(arg);
                            auto h     = arg_i->handle();

                            if (arg_i->device() == device_ptr)
                                return h;
                            else
                            {
                                // TODO : copy the value to the host from its device
                                // TODO : copy the value to the pipeline's device
                                return value_handle<Engine>();
                            }
                        }
                        else
                        {
                            static_assert(subtypes_list::template contains<std::decay_t<arg_type>>(),
                                          "Incorrect subtype");

                            return value_handle<Engine>();
                        }
                    },
                    item.arg_i);

                data_table.push_back(v);
            }
            else
            {
                auto& kernel = this->self()->device()->get_operation(item.operation_id);

                std::vector<value_handle<Engine>> op_inputs;
                {
                    op_inputs.reserve(item.input_nodes.size());
                    for (auto idx : item.input_nodes)
                    {
                        if (idx < data_table.size())
                            op_inputs.push_back(data_table[idx]);
                        else
                            throw std::runtime_error(
                                "Compilation is corrupted. The node depends on the following node.");
                    }
                }

                compute_context ctx;
                ctx.inputs = std::move(op_inputs);

                kernel.compute(ctx);

                data_table.push_back(ctx.output);
            }
        }
    }
}   // namespace dg::detail

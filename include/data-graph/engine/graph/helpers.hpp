
#ifndef __DATA_GRAPH__DETAIL__GRAPH__HELPERS_HPP__
#define __DATA_GRAPH__DETAIL__GRAPH__HELPERS_HPP__

#include <data-graph/type_traits.hpp>

#include <data-graph/detail/engine/helpers.hpp>

namespace dg::detail
{
    template <class Engine>
    struct graph_base;

    template <class Engine, class DataType>
    struct node_base;

    template <class Engine>
    struct graph_helpers final
    {
        static_assert(engine_helpers::is_engine_v<Engine>, "Provided `Engine` is not a valid engine.");

        using engine_type = Engine;

        using graph_type = typename engine_type::graph;

        /**
         * @class is_node
         * @brief
         *
         * @tparam Node
         *
         * @member type
         * @member value
         */
        template <class Node>
        struct is_node;

        template <class Node>
        static constexpr auto is_node_v = is_node<Node>::value;

        /**
         * @class
         * @brief
         *
         * @tparam Node
         *
         * @member type
         */
        template <class Node>
        struct get_node_data_type;

        template <class Node>
        using get_node_data_type_t = typename get_node_data_type<Node>::type;
    };
}   // namespace dg::detail

#include "./_impl/helpers.ipp"

#endif

#include <scoped_allocator>

namespace dg::detail
{
    namespace _impl
    {
        template <class Engine>
        struct graph_base_data_helper
        {
            using engine_allocator_t = typename Engine::allocator_type;

            using engine_data_type_id_t = typename Engine::data_type_id_type;
            using engine_operation_id_t = typename Engine::operation_id_type;

            using engine_subtype_idx_t  = typename Engine::subtype_idx_type;
            using engine_argument_idx_t = typename Engine::argument_idx_type;

            struct nodes_list_record;

            using node_list_allocator_t =
                typename std::allocator_traits<engine_allocator_t>::template rebind_alloc<nodes_list_record>;

            using nodes_list_t = std::list<nodes_list_record, std::scoped_allocator_adaptor<node_list_allocator_t>>;
            using nodes_list_iterator_t       = typename nodes_list_t::iterator;
            using nodes_list_const_iterator_t = typename nodes_list_t::const_iterator;

            // default_value

            struct default_value
            {
                template <typename T>
                default_value(engine_subtype_idx_t subtype_idx, T* data)
                    : _subtype_idx(subtype_idx), _data_ptr(reinterpret_cast<void*>(data))
                {}

                inline engine_subtype_idx_t subtype_idx() const { return _subtype_idx; }

                template <typename T>
                std::decay_t<T>* data()
                {
                    return reinterpret_cast<std::decay_t<T>*>(_data_ptr);
                }

                virtual ~default_value() {}

            private:
                engine_subtype_idx_t _subtype_idx;
                void* _data_ptr;   // not owning
            };

            struct default_value_deleter
            {
                using default_value_alloc_t =
                    typename std::allocator_traits<engine_allocator_t>::template rebind_alloc<default_value>;

                default_value_deleter() = default;
                default_value_deleter(default_value_alloc_t alloc) : _alloc(alloc) {}
                ~default_value_deleter() = default;

                void operator()(default_value* ptr)
                {
                    if (_alloc && ptr)
                    {
                        try
                        {
                            std::allocator_traits<default_value_alloc_t>::destroy(*_alloc, ptr);
                        }
                        catch (...)
                        {
                            std::allocator_traits<default_value_alloc_t>::deallocate(*_alloc, ptr, 1);
                        }
                    }
                }

            private:
                std::optional<default_value_alloc_t> _alloc;
            };

            using default_value_ptr_t = std::unique_ptr<default_value, default_value_deleter>;

            // make_default_value

            template <typename T>
            static default_value_ptr_t
                make_default_value(engine_allocator_t graph_alloc, engine_subtype_idx_t subtype_idx, T&& v)
            {
                using T_t = std::decay_t<T>;

                struct default_value_impl : public default_value
                {
                    default_value_impl(engine_subtype_idx_t subtype_idx, T&& v)
                        : default_value(subtype_idx, &_v), _v(std::forward<T>(v))
                    {}
                    virtual ~default_value_impl() override {}

                private:
                    T_t _v;
                };

                using default_value_alloc_t =
                    typename std::allocator_traits<engine_allocator_t>::template rebind_alloc<default_value_impl>;
                using alloc_traits =
                    typename std::allocator_traits<engine_allocator_t>::template rebind_traits<default_value_impl>;

                default_value_alloc_t alloc(graph_alloc);

                try
                {
                    auto ptr = alloc_traits::allocate(alloc, 1);
                    try
                    {
                        alloc_traits::construct(alloc, ptr, subtype_idx, std::forward<T>(v));
                        return default_value_ptr_t(ptr, default_value_deleter(alloc));
                    }
                    catch (...)
                    {
                        alloc_traits::deallocate(alloc, ptr, 1);
                        throw;
                    }
                }
                catch (std::bad_alloc)
                {
                    return {};
                }
            }

            // node_data

            struct node_data
            {
                engine_operation_id_t operation_id;
                engine_data_type_id_t output_type_id;
                std::basic_string<char, std::char_traits<char>, > name;
                default_value_ptr_t default_value;

                node_data(engine_operation_id_t op_id, engine_data_type_id_t out_type_id)
                    : operation_id(op_id), output_type_id(out_type_id)
                {}

                node_data(engine_operation_id_t op_id, engine_data_type_id_t out_type_id, std::string_view name)
                    : operation_id(op_id), output_type_id(out_type_id), name(name)
                {}

                node_data(engine_operation_id_t op_id, engine_data_type_id_t out_type_id, std::string&& name)
                    : operation_id(op_id), output_type_id(out_type_id), name(std::move(name))
                {}
            };

            // nodes_list_record

            struct nodes_list_record
            {
                using allocator_type =
                    typename std::allocator_traits<engine_allocator_t>::template rebind_alloc<nodes_list_iterator_t>;

                node_data node;
                std::vector<nodes_list_iterator_t, allocator_type> in_node_idxs;

                nodes_list_record(std::allocator_arg_t,
                                  allocator_type alloc,
                                  engine_operation_id_t op_id,
                                  engine_data_type_id_t out_type_id)
                    : node(op_id, out_type_id)
                {}
            };

            template <engine_argument_idx_t ArgIdx>
            static void bind_node(nodes_list_iterator_t& node_iter, nodes_list_iterator_t& in_node_iter)
            {
                node_iter->in_node_idxs[ArgIdx] = in_node_iter;
            }

            template <class ArgType, class ValueType, engine_argument_idx_t ArgIdx>
            static void bind_value(nodes_list_iterator_t& node_iter, ValueType&& value)
            {
                constexpr engine_subtype_idx_t subtype_id =
                    engine_helpers::data_type::get_subtypes_t<ArgType>::template index<std::decay_t<ValueType>>();

                node_iter->node.default_value =
                    make_default_value<ValueType>(subtype_id, std::forward<ValueType>(value));
            }

            // count_node_inputs

            template <class... Nodes>
            struct count_node_inputs
            {
                static constexpr std::size_t value =
                    reduce_v<reduce_ops::sum, 0, (graph_helpers<Engine>::template is_node_v<std::decay_t<Nodes>>) ...>;
            };

            template <class... Nodes>
            static constexpr auto count_node_inputs_v = count_node_inputs<Nodes...>::value;
        };
    }   // namespace _impl

    //
    // graph_base_internal
    //

    template <class Engine>
    struct graph_base_internal : interface_unique::internal<graph_base<Engine>>
    {
        using graph_helpers = typename graph_base<Engine>::graph_helpers;

        using allocator_type = typename Engine::allocator_type;

        using nodes_list_t                = typename _impl::graph_base_data_helper<Engine>::nodes_list_t;
        using nodes_list_iterator_t       = typename _impl::graph_base_data_helper<Engine>::nodes_list_iterator_t;
        using nodes_list_const_iterator_t = typename _impl::graph_base_data_helper<Engine>::nodes_list_const_iterator_t;

        graph_base_internal(const allocator_type& alloc) : _alloc(alloc) {}
        graph_base_internal(allocator_type&& alloc) : _alloc(std::move(alloc)) {}

        allocator_type allocator() const { return _alloc; }

        std::size_t n_nodes() const { return _nodes.size(); }

        std::size_t index_of(const nodes_list_const_iterator_t& it) const
        {
            nodes_list_const_iterator_t start = std::begin(_nodes);
            return std::distance(start, it);
        }

        std::size_t index_of(const nodes_list_iterator_t& it)
        {
            nodes_list_iterator_t start = std::begin(_nodes);
            return std::distance(start, it);
        }

        nodes_list_iterator_t end() { return std::end(_nodes); }
        nodes_list_const_iterator_t end() const { return std::end(_nodes); }

        // is_owned

        template <class DataType>
        inline bool is_owned(const node_base<Engine, DataType>& node) const;

        // append_identity_node

        template <class DataType>
        inline nodes_list_iterator_t append_identity_node(bool silent = false)
        {
            // counter++;
            using Operation = engine_helpers::operation::identity;
            _nodes.emplace_back(engine_helpers::operation::get_id_v<typename Engine::operations_list, Operation>,
                                engine_helpers::data_type::get_id_v<typename Engine::data_types_list, DataType>);

            auto node_iter = std::prev(_nodes.end());
            if (!silent) node_iter->in_node_idxs.resize(1);
            return node_iter;
        }

        // append_node

        template <class Operation, class DataType>
        inline nodes_list_iterator_t append_node()
        {
            // counter++;
            _nodes.emplace_back(engine_helpers::operation::get_id_v<typename Engine::operations_list, Operation>,
                                engine_helpers::data_type::get_id_v<typename Engine::data_types_list, DataType>);

            auto node_iter = std::prev(_nodes.end());
            node_iter->in_node_idxs.resize(engine_helpers::operation::get_input_types_t<Operation>::size());
            return node_iter;
        }

        // put_inputs

        template <class OperationArgsList,
                  class... NodeTypes,
                  typename = std::enable_if_t<OperationArgsList::size() == sizeof...(NodeTypes)>>
        void put_inputs(nodes_list_iterator_t& node_iter, NodeTypes&&... nodes)
        {
            using arg_pairs_type = types_list_zip_t<OperationArgsList, types_list<NodeTypes...>>;

            std::size_t errors_counter = 0;
            std::array<std::size_t, OperationArgsList::size()> error_args;

            auto nodes_tuple = std::forward_as_tuple(nodes...);
            static_for<arg_pairs_type::size()>(
                [this, &node_iter, &nodes_tuple, &errors_counter, &error_args](auto i) mutable {
                    using arg_pair_type = typename arg_pairs_type::template at_t<i>;

                    using ArgType  = typename arg_pair_type::template at_t<0>;
                    using NodeType = typename arg_pair_type::template at_t<1>;

                    if constexpr (graph_helpers::template is_node_v<std::decay_t<NodeType>>)
                    {
                        using node_data_type =
                            typename graph_helpers::template get_node_data_type_t<std::decay_t<NodeType>>;
                        static_assert(std::is_same_v<ArgType, node_data_type>, "Incorrect argument type");

                        auto& node_data = std::get<i>(nodes_tuple);

                        if (!node_data)
                        {
                            error_args[errors_counter] = i;
                            errors_counter++;
                        }
                        else
                        {
                            auto internal     = node_base_internal<Engine, node_data_type>::cast(node_data);
                            auto in_node_iter = internal->node_iter();

                            _impl::graph_base_data_helper<Engine>::template bind_node<i>(node_iter, in_node_iter);
                        }
                    }
                    else
                    {
                        static_assert(types_list_contains_v<std::decay_t<NodeType>, typename ArgType::types>,
                                      "Incorrect argument type");

                        auto in_node_iter = this->append_identity_node<ArgType>(true);

                        _impl::graph_base_data_helper<Engine>::template bind_value<ArgType, NodeType, i>(
                            in_node_iter, std::forward<NodeType>(std::get<i>(nodes_tuple)));

                        _impl::graph_base_data_helper<Engine>::template bind_node<i>(node_iter, in_node_iter);
                    }
                });

            if (errors_counter > 0)
            {
                // TODO : better error reporting
                throw std::runtime_error("Error occured");
            }
        }

    private:
        allocator_type _alloc;
        nodes_list_t _nodes;
    };   // namespace dg::detail

    //
    // node_base_internal
    //

    template <class Engine, class DataType>
    struct node_base_internal : interface_inline::internal<node_base<Engine, DataType>>
    {
        using nodes_list_iterator_t = typename _impl::graph_base_data_helper<Engine>::nodes_list_iterator_t;

        explicit node_base_internal(graph_base<Engine>& graph) : _graph(graph_base_internal<Engine>::cast(graph)) {}

        node_base_internal(graph_base<Engine>& graph, nodes_list_iterator_t node_iter)
            : _graph(graph_base_internal<Engine>::cast(graph)), _node_iter(node_iter)
        {}

        node_base_internal(const node_base_internal& o) : _graph(o._graph), _node_iter(o._node_iter) {}
        node_base_internal(node_base_internal&& o) noexcept
            : _graph(std::move(o._graph)), _node_iter(std::move(o._node_iter))
        {}

        inline node_base_internal& operator=(const node_base_internal& o)
        {
            _graph     = o._graph;
            _node_iter = o._node_iter;
            return *this;
        }

        inline node_base_internal& operator=(node_base_internal&& o) noexcept
        {
            _graph     = std::move(o._graph);
            _node_iter = std::move(o._node_iter);
            return *this;
        }

        inline bool operator==(const node_base_internal& o) const
        {
            return _graph == o._graph && _node_iter == o._node_iter;
        }

        inline bool operator!=(const node_base_internal& o) const
        {
            return _graph != o._graph || _node_iter != o._node_iter;
        }

        inline nodes_list_iterator_t& node_iter() { return _node_iter; }
        inline const nodes_list_iterator_t& node_iter() const { return _node_iter; }

        inline void node_iter(const nodes_list_iterator_t& it) { _node_iter = it; }

        graph_base_internal<Engine>* graph() { return _graph; }
        const graph_base_internal<Engine>* graph() const { return _graph; }

    private:
        graph_base_internal<Engine>* _graph;
        nodes_list_iterator_t _node_iter;
    };

    //
    // graph_base_internal
    //

    template <class Engine>
    template <class DataType>
    inline bool graph_base_internal<Engine>::is_owned(const node_base<Engine, DataType>& node) const
    {
        auto i = node_base_internal<Engine, DataType>::cast(node);
        return this == i->graph();
    }
}   // namespace dg::detail


namespace dg::detail
{
    namespace _impl
    {
        template <class Engine, class DataType>
        using node_interface_base = interface_inline ::base<node_base_internal<Engine, DataType>>;
    }

    template <class Engine, class DataType>
    node_base<Engine, DataType>::node_base()
    {}

    template <class Engine, class DataType>
    node_base<Engine, DataType>::node_base(graph_base<Engine>& parent)
        : _impl::node_interface_base<Engine, DataType>(std::in_place, parent)
    {}

    template <class Engine, class DataType>
    node_base<Engine, DataType>::node_base(const node_base& o) : _impl::node_interface_base<Engine, DataType>(o)
    {}

    template <class Engine, class DataType>
    node_base<Engine, DataType>::node_base(node_base&& o) noexcept
        : _impl::node_interface_base<Engine, DataType>(std::move(o))
    {}

    template <class Engine, class DataType>
    node_base<Engine, DataType>::~node_base()
    {}

    template <class Engine, class DataType>
    node_base<Engine, DataType>& node_base<Engine, DataType>::operator=(const node_base& o)
    {
        _impl::node_interface_base<Engine, DataType>::operator=(o);
        return *this;
    }

    template <class Engine, class DataType>
    node_base<Engine, DataType>& node_base<Engine, DataType>::operator=(node_base&& o) noexcept
    {
        _impl::node_interface_base<Engine, DataType>::operator=(std::move(o));
        return *this;
    }

    template <class Engine, class DataType>
    template <class AnotherDataType>
    inline constexpr bool node_base<Engine, DataType>::is()
    {
        return std::is_same_v<data_type, std::decay_t<AnotherDataType>>;
    }

    template <class Engine, class DataType>
    std::string_view node_base<Engine, DataType>::name() const
    {
        return bool(*this) ? this->self()->node_iter()->node.name : "";
    }

    template <class Engine, class DataType>
    inline bool node_base<Engine, DataType>::operator==(const node_base& o) const
    {
        return (*this->self()) == (*o.self());
    }

    template <class Engine, class DataType>
    inline bool node_base<Engine, DataType>::operator!=(const node_base& o) const
    {
        return (*this->self()) != (*o.self());
    }

    template <class Engine, class DataType>
    template <class AnotherType>
    inline bool node_base<Engine, DataType>::operator==(const node_base<Engine, AnotherType>& o) const
    {
        return false;
    }

    template <class Engine, class DataType>
    template <class AnotherType>
    inline bool node_base<Engine, DataType>::operator!=(const node_base<Engine, AnotherType>& o) const
    {
        return true;
    }
}   // namespace dg::detail

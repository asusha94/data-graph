
#ifndef __DATA_GRAPH__ENGINE__MAIN_HPP__
#define __DATA_GRAPH__ENGINE__MAIN_HPP__

#ifndef __DATA_GRAPH__ENGINE_HPP__
#    error "Please use `#include <data-graph/engine.hpp>` instead. This header is not supposed to be used directly."
#endif

#include <data-graph/engine/main/helpers.hpp>
#include <data-graph/engine/main/integrity.hpp>

namespace dg
{
    namespace declarations
    {
        template <typename... Ts>
        struct data_type final
        {
            using types = types_list<std::decay_t<Ts>...>;

            static constexpr std::size_t n_bytes = reduce_v<reduce_ops::max, (sizeof(std::decay_t<Ts>))...>;
        };

        template <typename Out, typename... Ins>
        struct operation<Out(Ins...)> final
        {
            using input_types = types_list<std::decay_t<Ins>...>;
            using output_type = std::decay_t<Out>;
        };
    }   // namespace declarations

    //
    // struct use_data_types
    //

    template <class... Types>
    struct use_data_types final
    {
        using types = typename types_list<std::decay_t<Types>...>::unique_t;
    };

    //
    // struct use_operations
    //

    template <class... Operations>
    struct use_operations final
    {
        using operations = typename types_list<std::decay_t<Operations>...>::unique_t;
    };

    //
    // struct engine
    //

    template <class DataTypesList, class OperationsList, class Allocator /* = std::allocator<std::byte> */>
    struct engine final
    {
        static_assert(detail::engine_integrity::is_valid_engine_v<DataTypesList, OperationsList>,
                      "Engine integrity check failed. "
                      "Please ensure that operations provided use specified types and all devices implement "
                      "these operations.");

        ///
        using allocator_type = Allocator;

        ///
        using data_types_list = typename detail::engine_helpers::data_type::get_types_list_t<DataTypesList>;

        ///
        using operations_list = typename detail::engine_helpers::operation::get_types_list_t<OperationsList>;

        /// A names table of registered data types
        static constexpr auto data_type_names = detail::engine_helpers::data_type::get_names_table_v<DataTypesList>;

        /// A names table of registered operations
        static constexpr auto operation_names = detail::engine_helpers::operation::get_names_table_v<OperationsList>;

        /// Holds a number of registered data types
        static constexpr auto n_data_types = data_types_list::size();

        /// Holds a number of registered operations
        static constexpr auto n_operations = operations_list::size();

        ///
        static constexpr auto max_n_subtypes = detail::engine_helpers::data_type::get_max_n_subtypes_v<DataTypesList>;

        ///
        static constexpr auto max_n_arguments =
            detail::engine_helpers::operation::get_max_n_arguments_v<OperationsList>;

        ///
        using data_type_id_type = uint_type_detector_t<n_data_types, true>;

        ///
        using operation_id_type = uint_type_detector_t<n_operations, true>;

        ///
        using subtype_idx_type = uint_type_detector_t<max_n_subtypes, true>;

        ///
        using argument_idx_type = uint_type_detector_t<max_n_arguments>;

        /**
         *
         * @tparam DataType
         * @return
         */
        template <class DataType>
        static constexpr bool data_type_is_allowed();

        /**
         *
         * @tparam Operation
         * @return
         */
        template <class Operation>
        static constexpr bool operation_is_allowed();

        /**
         *
         * @tparam Device
         * @return
         */
        template <class Device>
        static constexpr bool device_is_allowed();

        ///
        template <class DataType>
        using data_type_get_subtypes = typename detail::engine_helpers::data_type::template get_subtypes_t<DataType>;

        ///
        template <class Operation>
        using operation_get_input_types =
            typename detail::engine_helpers::operation::template get_input_types_t<Operation>;

        ///
        template <class Operation>
        using operation_get_output_type =
            typename detail::engine_helpers::operation::template get_output_type_t<Operation>;

        using graph = ::dg::graph<engine>;

        template <class DataType>
        using node = typename graph::template node<DataType>;

        using device = ::dg::device<engine>;

        template <class DataType>
        using value = typename device::template value<DataType>;
    };
}   // namespace dg

#include "./main.ipp"

#endif
